<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register Form</title>
</head>
<body>

<%@ include file="header.jsp" %>

<c:if test='${userJSP.id != 0 && !registerSuccess}'>
    <span style="color: red; ">Вы залогинены!</span>
</c:if>

<c:if test="${registerSuccess}">
    ${messageRegisterSuccess}
</c:if>

<c:if test='${userJSP.id == 0}'>
    <c:if test="${!registerSuccess}">
        <span style="color: red; ">${message}</span>

        <spring:form id="registerForm" method="post" action="register" modelAttribute="registerBeanJSP">

            <spring:label path="login">Логин</spring:label><br>
            <spring:input id="login" name="login" path="login" /><br>
            <spring:label path="password">Пароль</spring:label><br>
            <spring:password id="password" name="password" path="password" /><br>
            <spring:label path="confirmPassword">Подтверждение пароля</spring:label><br>
            <spring:password id="confirmPassword" name="confirmPassword" path="confirmPassword" /><br>
            <spring:label path="lastName">Фамилия</spring:label><br>
            <spring:input id="lastName" name="lastName" path="lastName" /><br>
            <spring:label path="firstName">Имя</spring:label><br>
            <spring:input id="firstName" name="firstName" path="firstName" /><br>
            <spring:label path="patronymic">Отчество</spring:label><br>
            <spring:input id="patronymic" name="patronymic" path="patronymic" /><br>
            <spring:label path="phone">Телефон</spring:label><br>
            <spring:input id="phone" name="phone" path="phone" /><br>
            <spring:label path="email">Email</spring:label><br>
            <spring:input id="email" name="email" path="email" /><br>
            <spring:label path="postcode">Почтовый код</spring:label><br>
            <spring:input id="postcode" name="postcode" path="postcode" /><br>
            <spring:label path="address">Адрес</spring:label><br>
            <spring:input id="address" name="address" path="address" /><br>
            <input type="submit" value="Регистрация" />

        </spring:form>
    </c:if>
</c:if>

</body>
</html>
