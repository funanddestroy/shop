<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${categoryJSP}</title>
</head>
<body>

<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>

${categoryJSP}

<br/>

<c:forEach var="good" items="${goodsJSP}">
    <c:if test='${good.availability || userJSP.role.equals("admin") || (userJSP.id == good.seller.id)}'>
        <c:forEach var="img" items="${good.images}">
            <img src="<c:url value="${img}"/>" width="400px" height="250px">
            <br/>
        </c:forEach>
        ${good}

        <c:if test="${userJSP.id != 0}">
            <br/>
            <spring:form action="${categoryJSP}" method="post">
                <input type="hidden" name="status" value="add_cart">
                <input type="hidden" name="good_id" value="${good.id}">
                <input type="number" name="quantityJSP" value="1" min="1" max="100" step="1">
                <input type="submit" value="Добавить в корзину"/>
            </spring:form>
            <c:if test='${userJSP.role.equals("admin") || good.seller.id == userJSP.id}'>
                <spring:form action="${categoryJSP}" method="post">
                    <input type="hidden" name="status" value="delete_good">
                    <input type="hidden" name="good_id" value="${good.id}">
                    <input type="submit" value="Удалить товар"/>
                </spring:form>
            </c:if>
        </c:if>
        <br/>
        <br/>
    </c:if>
</c:forEach>

</body>
</html>
