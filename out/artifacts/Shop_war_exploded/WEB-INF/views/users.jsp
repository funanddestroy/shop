<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
</head>
<body>

<%@ include file="header.jsp" %>

<c:if test='${userJSP.role.equals("admin")}'>

    <c:forEach var="us" items="${usersJSP}">
        ${us}<br/>
        <c:if test='${!us.login.equals("admin")}'>
            <spring:form action="users" method="post">
                <input type="hidden" name="user_id" value="${us.id}">
                <input type="hidden" name="status" value="set_role"/>
                <select name="roleJSP">
                    <option value="admin" ${us.role.equals("admin") ? 'selected="selected"' : ''}>Админ</option>
                    <option value="seller" ${us.role.equals("seller") ? 'selected="selected"' : ''}>Продавец</option>
                    <option value="user" ${us.role.equals("user") ? 'selected="selected"' : ''}>Пользователь</option>
                </select>
                <input type="submit" value="Изменить роль"/>
            </spring:form>
            <spring:form action="users" method="post">
                <input type="hidden" name="status" value="delete_user"/>
                <input type="hidden" name="user_id" value="${us.id}">
                <input type="submit" value="Удалить пользователя"/>
            </spring:form> <br/>
        </c:if>
        <br/>
    </c:forEach>

</c:if>

</body>
</html>
