<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<spr:url value="/" var="homeUrl"/>
<a href="${homeUrl}"> Домашняя </a>
||
<spr:url value="/category" var="categoryUrl"/>
<a href="${categoryUrl}"> Категории </a>
||

<c:if test="${userJSP.id == 0 || not empty massageLogout}">
    <spr:url value="/login" var="loginUrl"/>
    <a href="${loginUrl}"> Вход </a>
    ||
    <spr:url value="/register" var="registerUrl"/>
    <a href="${registerUrl}"> Регистрация </a>
</c:if>

<c:if test="${userJSP.id != 0 && empty massageLogout}">
    <spr:url value="/user" var="userUrl"/>
    <a href="${userUrl}"> Мой профиль </a>
    <c:if test='${userJSP.role.equals("admin")}'>
        ||
        <spr:url value="/users" var="usersUrl"/>
        <a href="${usersUrl}"> Пользователи </a>
    </c:if>
    <c:if test='${userJSP.role.equals("admin") || userJSP.role.equals("seller")}'>
        ||
        <spr:url value="/seller" var="sellerUrl"/>
        <a href="${sellerUrl}"> Мои товары </a>
    </c:if>
    ||
    <spr:url value="/logout" var="logoutUrl"/>
    <a href="${logoutUrl}"> Выход </a>
</c:if>

<br><br>
<c:if test="${userJSP.id != 0 && empty massageLogout}">
    ${userJSP.lastName} ${userJSP.firstName} ${userJSP.patronymic}
    ||
    <spr:url value="/goods" var="goodListUrl"/>
    <a href="${goodListUrl}"> Корзина </a>
    <br><br>
</c:if>

<hr>


