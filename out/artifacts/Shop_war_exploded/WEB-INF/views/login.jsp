<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Form</title>
</head>
<body>

<%@ include file="header.jsp" %>

<c:if test="${userJSP.id != 0 && !loginSuccess}">
    <span style="color: red; ">Вы уже залогинены!</span>
</c:if>

<c:if test="${loginSuccess}">
    ${messageLoginSuccess}
</c:if>

<c:if test="${userJSP.id == 0}">
    <c:if test="${!loginSuccess}">
        <span style="color: red; ">${message}</span>

        <spring:form id="loginForm" method="post" action="login" modelAttribute="loginBeanJSP">

            <spring:label path="userName">Логин</spring:label><br>
            <spring:input id="userName" name="userName" path="userName" /><br>
            <spring:label path="password">Пароль</spring:label><br>
            <spring:password id="password" name="password" path="password" /><br>
            <input type="submit" value="Вход" />

        </spring:form>
    </c:if>
</c:if>

</body>
</html>
