<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div id="category">
    ${lc.name}
    <ul>
        <c:if test="${not empty lc.subcategory}">
            <c:forEach var="sub" items="${lc.subcategory}">
                <c:if test='${sub.availability}'>
                    <li> ${sub.name} </li>

                    <ul>
                        <c:if test="${not empty sub.subcategory}">
                            <c:forEach var="s" items="${sub.subcategory}">
                                <spr:url value="/category/{slug}" var="articleUrl">
                                    <spr:param name="slug" value="${s.name}" />
                                </spr:url>
                                <c:if test='${s.availability}'>
                                    <li> <a href="${articleUrl}">  ${s.name} </a> </li>
                                </c:if>
                            </c:forEach>
                        </c:if>
                    </ul>
                </c:if>
            </c:forEach>
        </c:if>
    </ul>
</div>