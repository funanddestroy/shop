<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Edit Category</title>
</head>
<body>

<%@ include file="header.jsp" %>

<div id="category">
    ${lc.name}
    <c:if test='${userJSP.role.equals("admin")}'>
        <spring:form method="post" action="category">
            <input type="hidden" name="status" value="addCategory"/>
            <input type="hidden" name="parent" value="1"/>
            <label>Название</label><br>
            <input type="text" name="name"/><br>
            <label>Описание</label><br>
            <input type="text" name="description"/><br>
            <input type="submit" value="Добавить" />
        </spring:form>
    </c:if>
    <ul>
        <c:if test="${not empty lc.subcategory}">
            <c:forEach var="sub" items="${lc.subcategory}">
                <c:if test='${sub.availability || userJSP.role.equals("admin")}'>
                    <li> ${sub.name}
                        <c:if test='${userJSP.role.equals("admin")}'>
                            <spring:form method="post" action="category">
                                <input type="hidden" name="status" value="addCategory"/>
                                <input type="hidden" name="parent" value="${sub.id}"/>
                                <label>Название</label><br>
                                <input type="text" name="name"/><br>
                                <label>Описание</label><br>
                                <input type="text" name="description"/><br>
                                <input type="submit" value="Добавить" />
                            </spring:form>
                            <c:if test="${empty sub.subcategory}">
                                <spring:form method="post" action="category">
                                    <input type="hidden" name="status" value="deleteCategory"/>
                                    <input type="hidden" name="category_id" value="${sub.id}"/>
                                    <input type="submit" value="Удалить" />
                                </spring:form>
                            </c:if>
                            <c:if test="${sub.availability}">
                                <spring:form method="post" action="category">
                                    <input type="hidden" name="status" value="hideCategory"/>
                                    <input type="hidden" name="category_id" value="${sub.id}"/>
                                    <input type="submit" value="Скрыть" />
                                </spring:form>
                            </c:if>
                            <c:if test="${not sub.availability}">
                                <spring:form method="post" action="category">
                                    <input type="hidden" name="status" value="showCategory"/>
                                    <input type="hidden" name="category_id" value="${sub.id}"/>
                                    <input type="submit" value="Показать" />
                                </spring:form>
                            </c:if>
                        </c:if>
                    </li>
                    <ul>
                        <c:if test="${not empty sub.subcategory}">
                            <c:forEach var="s" items="${sub.subcategory}">
                                <c:if test='${s.availability || userJSP.role.equals("admin")}'>
                                    <spr:url value="/category/{slug}" var="articleUrl">
                                        <spr:param name="slug" value="${s.name}" />
                                    </spr:url>

                                    <li> <a href="${articleUrl}">  ${s.name} </a>
                                        <c:if test='${userJSP.role.equals("admin")}'>
                                            <spring:form method="post" action="category">
                                                <input type="hidden" name="status" value="deleteCategory"/>
                                                <input type="hidden" name="category_id" value="${s.id}"/>
                                                <input type="submit" value="Удалить" />
                                            </spring:form>
                                            <c:if test="${s.availability}">
                                                <spring:form method="post" action="category">
                                                    <input type="hidden" name="status" value="hideCategory"/>
                                                    <input type="hidden" name="category_id" value="${s.id}"/>
                                                    <input type="submit" value="Скрыть" />
                                                </spring:form>
                                            </c:if>
                                            <c:if test="${not s.availability}">
                                                <spring:form method="post" action="category">
                                                    <input type="hidden" name="status" value="showCategory"/>
                                                    <input type="hidden" name="category_id" value="${s.id}"/>
                                                    <input type="submit" value="Показать" />
                                                </spring:form>
                                            </c:if>
                                        </c:if>
                                    </li>
                                </c:if>
                            </c:forEach>
                        </c:if>
                    </ul>
                </c:if>
            </c:forEach>
        </c:if>
    </ul>
</div>


</body>
</html>
