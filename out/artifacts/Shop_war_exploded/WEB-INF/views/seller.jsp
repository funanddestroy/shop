<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Seller</title>
</head>
<body>

<%@ include file="header.jsp" %>

<c:if test='${userJSP.role.equals("admin") || userJSP.role.equals("seller")}'>

    <spring:form method="get" action="new_good">
        <input type="submit" value="Добавить товар" />
    </spring:form>

    <spring:form method="get" action="good_status">
        <input type="submit" value="Статус товара" />
    </spring:form>

    <c:forEach var="good" items="${goodsJSP}">
        <c:forEach var="img" items="${good.images}">
            <img src="<c:url value="${img}"/>" width="200px" height="125px">
            <br/>
        </c:forEach>
        ${good}<br/>
        <spring:form action="seller" method="post">
            <input type="hidden" name="status" value="deleteGood"/>
            <input type="hidden" name="good_id" value="${good.id}"/>
            <input type="submit" value="Удалить товар"/>
        </spring:form>
        <c:if test="${good.availability}">
            <spring:form method="post" action="seller">
                <input type="hidden" name="status" value="hideGood"/>
                <input type="hidden" name="good_id" value="${good.id}"/>
                <input type="submit" value="Скрыть" />
            </spring:form>
        </c:if>
        <c:if test="${not good.availability}">
            <spring:form method="post" action="seller">
                <input type="hidden" name="status" value="showGood"/>
                <input type="hidden" name="good_id" value="${good.id}"/>
                <input type="submit" value="Показать" />
            </spring:form>
        </c:if>
        <br/>
        <br/>
    </c:forEach>
</c:if>

</body>
</html>
