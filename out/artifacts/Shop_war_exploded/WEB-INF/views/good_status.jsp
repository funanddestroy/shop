<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Good Status</title>
</head>
<body>

<%@ include file="header.jsp" %>

<hr>
<c:forEach var="pair" items="${gqJSP}">
    Товар:<br>
    <c:forEach var="good" items="${pair.element0}">
        ${good} <br>
        <br>
    </c:forEach>
    Пользователь:<br>
    ${pair.element1}
    <hr>
</c:forEach>


</body>
</html>
