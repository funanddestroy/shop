package ru.shop.dao;

import ru.shop.model.Category;

import java.util.List;

public interface CategoryDao {

    List<Category> getByParent(int parent);

    int getId(String name);

    Category get(String name);

    void addCategory(Category category);

    boolean existCategory(String name);

    void hideCategory(int categoryId);

    void showCategory(int categoryId);
}
