package ru.shop.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.shop.dao.ShopDao;
import ru.shop.dao.GoodDao;
import ru.shop.model.Good;
import ru.shop.model.Pair;

import java.util.List;

@Repository
public class GoodDaoImpl implements GoodDao, ShopDao<Good> {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    public void saveOrUpdate(Good good) {
        if (good.getId() > 0) {
            // update
            String sql = "UPDATE good SET category_id=?, name=?, manufacturer=?, description=?, model=?, price=?, discount=?, availability=?, quantity=?, seller=? WHERE id=?";
            jdbcTemplate.update(sql, good.getCategory().getId(), good.getName(), good.getManufacturer(),
                    good.getDescription(), good.getModel(), good.getPrice(), good.getDiscount(),
                    good.getAvailability(), good.getQuantity(), good.getSeller().getId(), good.getId());
        } else {
            // insert
            String sql = "INSERT INTO good (category_id, name, manufacturer, description, model, price, discount, availability, quantity, seller)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            jdbcTemplate.update(sql, good.getCategory().getId(), good.getName(), good.getManufacturer(),
                    good.getDescription(), good.getModel(), good.getPrice(), good.getDiscount(),
                    good.getAvailability(), good.getQuantity(), good.getSeller().getId());
        }
    }

    @Override
    public void delete(int goodId) {
        String sql = "DELETE FROM good WHERE id=?";
        jdbcTemplate.update(sql, goodId);
    }

    @Override
    public Good get(int goodId) {
        String sql = "SELECT * FROM good WHERE id=" + goodId;
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                Good good = new Good();

                CategoryDaoImpl categoryDAO = new CategoryDaoImpl();
                categoryDAO.setJdbcTemplate(jdbcTemplate);

                good.setId(rs.getInt("id"));
                good.setCategory(categoryDAO.get(rs.getInt("category_id")));
                good.setName(rs.getString("name"));
                good.setManufacturer(rs.getString("manufacturer"));
                good.setDescription(rs.getString("description"));
                good.setModel(rs.getString("model"));
                good.setPrice(rs.getDouble("price"));
                good.setDiscount(rs.getInt("discount"));
                good.setAvailability(rs.getBoolean("availability"));
                good.setQuantity(rs.getInt("quantity"));

                String sqlImg = "SELECT * FROM image WHERE good_id=" + good.getId();
                List<String> listImg = jdbcTemplate.query(sqlImg, (rs1, rowNum1) -> rs1.getString("image_path"));

                good.setImages(listImg);

                UserDaoImpl userDao = new UserDaoImpl();
                userDao.setJdbcTemplate(jdbcTemplate);

                good.setSeller(userDao.get(rs.getInt("seller")));

                return good;
            }
            return null;
        });
    }

    @Override
    public List<Good> list() {
        String sql = "SELECT * FROM good";
        List<Good> listGoods = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Good good = new Good();

            CategoryDaoImpl categoryDAO = new CategoryDaoImpl();
            categoryDAO.setJdbcTemplate(jdbcTemplate);

            good.setId(rs.getInt("id"));
            good.setCategory(categoryDAO.get(rs.getInt("category_id")));
            good.setName(rs.getString("name"));
            good.setManufacturer(rs.getString("manufacturer"));
            good.setDescription(rs.getString("description"));
            good.setModel(rs.getString("model"));
            good.setPrice(rs.getDouble("price"));
            good.setDiscount(rs.getInt("discount"));
            good.setAvailability(rs.getBoolean("availability"));
            good.setQuantity(rs.getInt("quantity"));

            String sqlImg = "SELECT * FROM image WHERE good_id=" + good.getId();
            List<String> listImg = jdbcTemplate.query(sqlImg, (rs1, rowNum1) -> rs1.getString("image_path"));

            good.setImages(listImg);

            UserDaoImpl userDao = new UserDaoImpl();
            userDao.setJdbcTemplate(jdbcTemplate);

            good.setSeller(userDao.get(rs.getInt("seller")));

            return good;
        });

        return listGoods;
    }

    @Override
    public List<Good> getByCategory(int categoryId) {
        String sql = "SELECT * FROM good WHERE category_id=" + categoryId;
        List<Good> listGoods = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Good good = new Good();

            CategoryDaoImpl categoryDAO = new CategoryDaoImpl();
            categoryDAO.setJdbcTemplate(jdbcTemplate);

            good.setId(rs.getInt("id"));
            good.setCategory(categoryDAO.get(rs.getInt("category_id")));
            good.setName(rs.getString("name"));
            good.setManufacturer(rs.getString("manufacturer"));
            good.setDescription(rs.getString("description"));
            good.setModel(rs.getString("model"));
            good.setPrice(rs.getDouble("price"));
            good.setDiscount(rs.getInt("discount"));
            good.setAvailability(rs.getBoolean("availability"));
            good.setQuantity(rs.getInt("quantity"));

            String sqlImg = "SELECT * FROM image WHERE good_id=" + good.getId();
            List<String> listImg = jdbcTemplate.query(sqlImg, (rs1, rowNum1) -> rs1.getString("image_path"));

            good.setImages(listImg);

            UserDaoImpl userDao = new UserDaoImpl();
            userDao.setJdbcTemplate(jdbcTemplate);

            good.setSeller(userDao.get(rs.getInt("seller")));

            return good;
        });

        return listGoods;
    }

    @Override
    public List<Pair<Good, Integer>> getByUserInCart(int userId) {
        String sql = "SELECT good.id, good.category_id, good.name, good.manufacturer, good.description, good.model, good.price, good.discount, good.availability, good.quantity, good.seller, good_list.quantaty as quantaty_gl FROM good JOIN good_list ON (good_list.good_id = good.id) JOIN \"order\" ON (good_list.order_id = \"order\".id) WHERE \"order\".status = 'В корзине' AND \"order\".user_id = " + userId;
        List<Pair<Good, Integer>> listGoods = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Good good = new Good();

            CategoryDaoImpl categoryDAO = new CategoryDaoImpl();
            categoryDAO.setJdbcTemplate(jdbcTemplate);

            good.setId(rs.getInt("id"));
            good.setCategory(categoryDAO.get(rs.getInt("category_id")));
            good.setName(rs.getString("name"));
            good.setManufacturer(rs.getString("manufacturer"));
            good.setDescription(rs.getString("description"));
            good.setModel(rs.getString("model"));
            good.setPrice(rs.getDouble("price"));
            good.setDiscount(rs.getInt("discount"));
            good.setAvailability(rs.getBoolean("availability"));
            good.setQuantity(rs.getInt("quantity"));

            String sqlImg = "SELECT * FROM image WHERE good_id=" + good.getId();
            List<String> listImg = jdbcTemplate.query(sqlImg, (rs1, rowNum1) -> rs1.getString("image_path"));

            good.setImages(listImg);

            UserDaoImpl userDao = new UserDaoImpl();
            userDao.setJdbcTemplate(jdbcTemplate);

            good.setSeller(userDao.get(rs.getInt("seller")));

            int quantity = rs.getInt("quantaty_gl");

            return Pair.createPair(good, quantity);
        });

        return listGoods;
    }

    @Override
    public List<Good> getBySeller(int sellerId) {
        String sql = "SELECT * FROM good WHERE seller=" + sellerId;
        List<Good> listGoods = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Good good = new Good();

            CategoryDaoImpl categoryDAO = new CategoryDaoImpl();
            categoryDAO.setJdbcTemplate(jdbcTemplate);

            good.setId(rs.getInt("id"));
            good.setCategory(categoryDAO.get(rs.getInt("category_id")));
            good.setName(rs.getString("name"));
            good.setManufacturer(rs.getString("manufacturer"));
            good.setDescription(rs.getString("description"));
            good.setModel(rs.getString("model"));
            good.setPrice(rs.getDouble("price"));
            good.setDiscount(rs.getInt("discount"));
            good.setAvailability(rs.getBoolean("availability"));
            good.setQuantity(rs.getInt("quantity"));

            String sqlImg = "SELECT * FROM image WHERE good_id=" + good.getId();
            List<String> listImg = jdbcTemplate.query(sqlImg, (rs1, rowNum1) -> rs1.getString("image_path"));

            good.setImages(listImg);

            UserDaoImpl userDao = new UserDaoImpl();
            userDao.setJdbcTemplate(jdbcTemplate);

            good.setSeller(userDao.get(rs.getInt("seller")));

            return good;
        });

        return listGoods;
    }

    @Override
    public void addGood(Good good) {
        good.setId(0);
        saveOrUpdate(good);
    }

    @Override
    public void hideGood(int goodId) {
        String sql = "UPDATE good SET availability=? WHERE id=?";
        jdbcTemplate.update(sql, false, goodId);
    }

    @Override
    public void showGood(int goodId) {
        String sql = "UPDATE good SET availability=? WHERE id=?";
        jdbcTemplate.update(sql, true, goodId);
    }
}
