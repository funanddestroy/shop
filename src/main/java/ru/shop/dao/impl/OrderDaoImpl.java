package ru.shop.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.shop.dao.OrderDao;
import ru.shop.dao.ShopDao;
import ru.shop.model.*;

import java.util.List;

public class OrderDaoImpl implements OrderDao, ShopDao<Order> {

    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void saveOrUpdate(Order order) {
        if (order.getId() > 0) {
            // update
            String sql = "UPDATE \"order\" SET user_id=?, order_price=?, delivery_method=?, payment_method=?, status=? WHERE id=?";
            jdbcTemplate.update(sql, order.getUser().getId(), order.getPrice(), order.getDeliveryMethod(),
                    order.getPaymentMethod(), order.getStatus(), order.getId());
        } else {
            // insert
            String sql = "INSERT INTO \"order\" (user_id, order_price, delivery_method, payment_method, status)"
                    + " VALUES (?, ?, ?, ?, ?)";
            jdbcTemplate.update(sql, order.getUser().getId(), order.getPrice(), order.getDeliveryMethod(),
                    order.getPaymentMethod(), order.getStatus());
        }
    }

    @Override
    public void delete(int orderId) {
        String sql = "DELETE FROM \"order\" WHERE id=?";
        jdbcTemplate.update(sql, orderId);
    }

    @Override
    public Order get(int orderId) {
        String sql = "SELECT * FROM \"order\" WHERE id=" + orderId;
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("id"));

                UserDaoImpl userDAO = new UserDaoImpl();
                userDAO.setJdbcTemplate(jdbcTemplate);
                order.setUser(userDAO.get(rs.getInt("user_id")));

                GoodDaoImpl goodDAO = new GoodDaoImpl();
                goodDAO.setJdbcTemplate(jdbcTemplate);
                String sqlGoods = "SELECT * FROM good_list WHERE order_id=" + order.getId();
                List<GoodQuantity> goodList = jdbcTemplate.query(sqlGoods, (rsGood, rowNumGood) -> {
                    GoodQuantity goodQuantaty = new GoodQuantity();
                    goodQuantaty.setGood(goodDAO.get(rsGood.getInt("good_id")));
                    goodQuantaty.setQuantity(rsGood.getInt("quantaty"));
                    return goodQuantaty;
                });
                order.setGoods(goodList);

                order.setPrice(rs.getDouble("order_price"));
                order.setDeliveryMethod(rs.getString("delivery_method"));
                order.setPaymentMethod(rs.getString("payment_method"));
                order.setStatus(rs.getString("status"));

                return order;
            }

            return null;
        });

    }

    @Override
    public List<Order> list() {
        String sql = "SELECT * FROM \"order\"";
        List<Order> listCategories = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Order order = new Order();
            order.setId(rs.getInt("id"));

            UserDaoImpl userDAO = new UserDaoImpl();
            userDAO.setJdbcTemplate(jdbcTemplate);
            order.setUser(userDAO.get(rs.getInt("user_id")));

            GoodDaoImpl goodDAO = new GoodDaoImpl();
            goodDAO.setJdbcTemplate(jdbcTemplate);
            String sqlGoods = "SELECT * FROM good_list WHERE order_id=" + order.getId();
            List<GoodQuantity> goodList = jdbcTemplate.query(sqlGoods, (rsGood, rowNumGood) -> {
                GoodQuantity goodQuantaty = new GoodQuantity();
                goodQuantaty.setGood(goodDAO.get(rsGood.getInt("good_id")));
                goodQuantaty.setQuantity(rsGood.getInt("quantaty"));
                return goodQuantaty;
            });
            order.setGoods(goodList);

            order.setPrice(rs.getDouble("order_price"));
            order.setDeliveryMethod(rs.getString("delivery_method"));
            order.setPaymentMethod(rs.getString("payment_method"));
            order.setStatus(rs.getString("status"));

            return order;
        });

        return listCategories;
    }

    @Override
    public List<Order> getByUser(User user) {
        String sql = "SELECT * FROM \"order\" WHERE user_id=" + user.getId();
        List<Order> listCategories = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Order order = new Order();
            order.setId(rs.getInt("id"));

            UserDaoImpl userDAO = new UserDaoImpl();
            userDAO.setJdbcTemplate(jdbcTemplate);
            order.setUser(userDAO.get(rs.getInt("user_id")));

            GoodDaoImpl goodDAO = new GoodDaoImpl();
            goodDAO.setJdbcTemplate(jdbcTemplate);
            String sqlGoods = "SELECT * FROM good_list WHERE order_id=" + order.getId();
            List<GoodQuantity> goodList = jdbcTemplate.query(sqlGoods, (rsGood, rowNumGood) -> {
                GoodQuantity goodQuantaty = new GoodQuantity();
                goodQuantaty.setGood(goodDAO.get(rsGood.getInt("good_id")));
                goodQuantaty.setQuantity(rsGood.getInt("quantaty"));
                return goodQuantaty;
            });
            order.setGoods(goodList);

            order.setPrice(rs.getDouble("order_price"));
            order.setDeliveryMethod(rs.getString("delivery_method"));
            order.setPaymentMethod(rs.getString("payment_method"));
            order.setStatus(rs.getString("status"));

            return order;
        });

        return listCategories;
    }

    @Override
    public List<Order> getByUserAndStatus(User user, String status) {
        String sql = "SELECT * FROM \"order\" WHERE user_id=" + user.getId() + " AND status='" + status + "'";
        List<Order> listCategories = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Order order = new Order();
            order.setId(rs.getInt("id"));

            UserDaoImpl userDAO = new UserDaoImpl();
            userDAO.setJdbcTemplate(jdbcTemplate);
            order.setUser(userDAO.get(rs.getInt("user_id")));

            GoodDaoImpl goodDAO = new GoodDaoImpl();
            goodDAO.setJdbcTemplate(jdbcTemplate);
            String sqlGoods = "SELECT * FROM good_list WHERE order_id=" + order.getId();
            List<GoodQuantity> goodList = jdbcTemplate.query(sqlGoods, (rsGood, rowNumGood) -> {
                GoodQuantity goodQuantity = new GoodQuantity();
                goodQuantity.setGood(goodDAO.get(rsGood.getInt("good_id")));
                goodQuantity.setQuantity(rsGood.getInt("quantaty"));
                return goodQuantity;
            });
            order.setGoods(goodList);

            order.setPrice(rs.getDouble("order_price"));
            order.setDeliveryMethod(rs.getString("delivery_method"));
            order.setPaymentMethod(rs.getString("payment_method"));
            order.setStatus(rs.getString("status"));

            return order;
        });

        return listCategories;
    }

    @Override
    public List<Pair<List<GoodQuantity>, User>> getOrders(String status, int sellerId) {
        String sql = "SELECT \"order\".id, \"order\".user_id FROM good JOIN good_list ON (good_list.good_id = good.id) JOIN \"order\" ON (good_list.order_id = \"order\".id) WHERE \"order\".status = '" + status + "' AND good.seller = " + sellerId + " GROUP BY \"order\".id";
        List<Pair<List<GoodQuantity>, User>> listOrders = jdbcTemplate.query(sql, (rs, rowNum) -> {

            String sqlGood = "SELECT good.id, good.category_id, good.name, good.manufacturer, good.description, good.model, good.price, good.discount, good.availability, good.quantity, good.seller, good_list.quantaty as quantaty_gl FROM good JOIN good_list ON (good_list.good_id = good.id) JOIN \"order\" ON (good_list.order_id = \"order\".id) WHERE \"order\".status = '" + status + "' AND \"order\".id = " + rs.getInt("id") + " AND good.seller = " + sellerId;
            List<GoodQuantity> listGoods = jdbcTemplate.query(sqlGood, (rsg, rowNumg) -> {
                Good good = new Good();

                CategoryDaoImpl categoryDAO = new CategoryDaoImpl();
                categoryDAO.setJdbcTemplate(jdbcTemplate);

                good.setId(rsg.getInt("id"));
                good.setCategory(categoryDAO.get(rsg.getInt("category_id")));
                good.setName(rsg.getString("name"));
                good.setManufacturer(rsg.getString("manufacturer"));
                good.setDescription(rsg.getString("description"));
                good.setModel(rsg.getString("model"));
                good.setPrice(rsg.getDouble("price"));
                good.setDiscount(rsg.getInt("discount"));
                good.setAvailability(rsg.getBoolean("availability"));
                good.setQuantity(rsg.getInt("quantity"));

                String sqlImg = "SELECT * FROM image WHERE good_id=" + good.getId();
                List<String> listImg = jdbcTemplate.query(sqlImg, (rs1, rowNum1) -> rs1.getString("image_path"));

                good.setImages(listImg);

                UserDaoImpl userDao = new UserDaoImpl();
                userDao.setJdbcTemplate(jdbcTemplate);

                good.setSeller(userDao.get(rsg.getInt("seller")));

                int quantity = rsg.getInt("quantaty_gl");

                GoodQuantity goodQuantity = new GoodQuantity();
                goodQuantity.setGood(good);
                goodQuantity.setQuantity(quantity);

                return goodQuantity;
            });

            UserDaoImpl userDao = new UserDaoImpl();
            userDao.setJdbcTemplate(jdbcTemplate);

            return new Pair<>(listGoods, userDao.get(rs.getInt("user_id")));
        });

        return listOrders;
    }

    @Override
    public void createOrder(User user) {
        if (!getByUserAndStatus(user, "В корзине").isEmpty()) {
            return;
        }

        Order order = new Order();
        order.setStatus("В корзине");
        order.setUser(user);
        order.setPrice(0.0);
        order.setDeliveryMethod("");
        order.setPaymentMethod("");

        saveOrUpdate(order);
    }

    @Override
    public void addGood(User user, int goodId, int quantity) {
        createOrder(user);

        String sql = "SELECT id FROM \"order\" WHERE status = 'В корзине' AND user_id = " + user.getId();
        int orderId = jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        });

        if (!isGoodInOrder(goodId, orderId)) {
            sql = "INSERT INTO good_list(good_id, order_id, quantaty)"
                    + " VALUES (?, ?, ?)";
            jdbcTemplate.update(sql, goodId, orderId, quantity);
        }
    }

    @Override
    public void deleteGood(User user, int goodId) {
        String sql = "SELECT id FROM \"order\" WHERE status = 'В корзине' AND user_id = " + user.getId();
        int orderId = jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        });

        sql = "DELETE FROM good_list WHERE order_id = ? AND good_id = ?";
        jdbcTemplate.update(sql, orderId, goodId);
    }

    @Override
    public void ordering(User user, double price, String deliveryMethod, String paymentMethod) {
        String sql = "SELECT id FROM \"order\" WHERE status = 'В корзине' AND user_id = " + user.getId();
        int orderId = jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        });

        Order order = new Order();
        order.setId(orderId);
        order.setStatus("Оформлен");
        order.setUser(user);
        order.setPrice(price);
        order.setDeliveryMethod(deliveryMethod);
        order.setPaymentMethod(paymentMethod);

        saveOrUpdate(order);
    }

    @Override
    public boolean isGoodInOrder(int goodId, int orderId) {
        String sql = "SELECT count(1) FROM good_list WHERE good_id = ? AND order_id = ?;";
        return jdbcTemplate.query(sql, new Object[]{goodId, orderId}, rs -> {
            if (rs.next()) {
                return (rs.getInt(1) > 0);
            }
            return  false;
        });

    }
}
