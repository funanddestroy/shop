package ru.shop.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.shop.dao.ShopDao;
import ru.shop.dao.UserDao;
import ru.shop.model.User;

import java.sql.SQLException;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao, ShopDao<User> {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    public void saveOrUpdate(User user) {
        if (user.getId() > 0) {
            // update
            String sql = "UPDATE \"user\" SET login=?, password=?, role=?, first_name=?, last_name=?, patronymic=?, phone=?, email=?, postcode=?, address=? WHERE id=?";
            jdbcTemplate.update(sql, user.getLogin(), user.getPassword(), user.getRole(), user.getFirstName(),
                    user.getLastName(), user.getPatronymic(), user.getPhone(), user.getEmail(),
                    user.getPostcode(), user.getAddress(), user.getId());
        } else {
            // insert
            String sql = "INSERT INTO \"user\" (login, password, role, first_name, last_name, patronymic, phone, email, postcode, address)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            jdbcTemplate.update(sql, user.getLogin(), user.getPassword(), user.getRole(), user.getFirstName(),
                    user.getLastName(), user.getPatronymic(), user.getPhone(), user.getEmail(),
                    user.getPostcode(), user.getAddress());
        }
    }

    @Override
    public void delete(int userId) {
        if (userId == 1) {
            return;
        }
        String sql = "DELETE FROM \"user\" WHERE id=?";
        jdbcTemplate.update(sql, userId);
    }

    @Override
    public User get(int userId) {
        String sql = "SELECT * FROM \"user\" WHERE id=" + userId;
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                User user = new User();

                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setRole(rs.getString("role"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setPatronymic(rs.getString("patronymic"));
                user.setPhone(rs.getString("phone"));
                user.setEmail(rs.getString("email"));
                user.setPostcode(rs.getString("postcode"));
                user.setAddress(rs.getString("address"));

                return user;
            }
            return  null;
        });
    }

    @Override
    public List<User> list() {
        String sql = "SELECT * FROM \"user\"";
        List<User> listUsers = jdbcTemplate.query(sql, (rs, rowNum) -> {
            User user = new User();

            user.setId(rs.getInt("id"));
            user.setLogin(rs.getString("login"));
            user.setPassword(rs.getString("password"));
            user.setRole(rs.getString("role"));
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));
            user.setPatronymic(rs.getString("patronymic"));
            user.setPhone(rs.getString("phone"));
            user.setEmail(rs.getString("email"));
            user.setPostcode(rs.getString("postcode"));
            user.setAddress(rs.getString("address"));

            return user;
        });

        return listUsers;
    }

    @Override
    public User getByLogin(String login) {
        String sql = "SELECT * FROM \"user\" WHERE \"login\"='" + login + "'";
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                User user = new User();

                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setRole(rs.getString("role"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setPatronymic(rs.getString("patronymic"));
                user.setPhone(rs.getString("phone"));
                user.setEmail(rs.getString("email"));
                user.setPostcode(rs.getString("postcode"));
                user.setAddress(rs.getString("address"));

                return user;
            }
            return  null;
        });
    }

    @Override
    public void addUser(User user) throws SQLException {
        if (existUser(user.getLogin()))
        {
            return;
        }
        user.setId(0);
        saveOrUpdate(user);
    }

    @Override
    public boolean isValidUser(String username, String password) throws SQLException {
        String sql = "SELECT count(1) FROM \"user\" WHERE login = ? AND password = ?";
        return jdbcTemplate.query(sql, new Object[]{username, password}, rs -> {
            if (rs.next()) {
                return (rs.getInt(1) > 0);
            }
            return  false;
        });
    }

    @Override
    public boolean existUser(String username) throws SQLException {
        String sql = "SELECT count(1) FROM \"user\" WHERE login = ?";
        return jdbcTemplate.query(sql, new Object[]{username}, rs -> {
            if (rs.next()) {
                return (rs.getInt(1) > 0);
            }
            return  false;
        });
    }

    @Override
    public void setRole(int userId, String role) {
        User user = get(userId);
        user.setRole(role);
        saveOrUpdate(user);
    }
}
