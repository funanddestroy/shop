package ru.shop.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.shop.dao.ShopDao;
import ru.shop.dao.CategoryDao;
import ru.shop.model.Category;
import ru.shop.model.Good;

import java.util.List;

@Repository
public class CategoryDaoImpl implements CategoryDao,ShopDao<Category> {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    public void saveOrUpdate(Category category) {
        if (category.getId() > 0) {
            // update
            String sql = "UPDATE category SET name=?, description=?, availability=?, parent=? WHERE id=?";
            jdbcTemplate.update(sql, category.getName(), category.getDescription(),
                    category.getAvailability(), category.getParent(), category.getId());
        } else {
            // insert
            String sql = "INSERT INTO category (name, description, availability, parent)"
                    + " VALUES (?, ?, ?, ?)";
            jdbcTemplate.update(sql, category.getName(), category.getDescription(),
                    category.getAvailability(), category.getParent());
        }
    }

    @Override
    public void delete(int categoryId) {
        if (categoryId == 1) {
            return;
        }

        GoodDaoImpl goodDao = new GoodDaoImpl();
        goodDao.setJdbcTemplate(jdbcTemplate);

        List<Good> lg = goodDao.getByCategory(categoryId);
        for (Good good : lg) {
            goodDao.delete(good.getId());
        }

        String sql = "DELETE FROM category WHERE id=?";
        jdbcTemplate.update(sql, categoryId);
    }

    @Override
    public Category get(final int categoryId) {
        String sql = "SELECT * FROM category WHERE id=" + categoryId;
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                Category category = new Category();
                category.setId(rs.getInt("id"));
                category.setName(rs.getString("name"));
                category.setDescription(rs.getString("description"));
                category.setAvailability(rs.getBoolean("availability"));
                category.setParent(rs.getInt("parent"));

                category.setSubcategory(getByParent(categoryId));
                return category;
            }

            return null;
        });
    }

    @Override
    public List<Category> getByParent(final int parent) {
        String sql = "SELECT * FROM category WHERE parent=" + parent;
        List<Category> listCategories = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Category category = new Category();
            category.setId(rs.getInt("id"));
                category.setName(rs.getString("name"));
            category.setDescription(rs.getString("description"));
            category.setAvailability(rs.getBoolean("availability"));
            category.setParent(rs.getInt("parent"));

            category.setSubcategory(getByParent(category.getId()));

            return category;
        });

        return listCategories;
    }

    @Override
    public List<Category> list() {
        String sql = "SELECT * FROM category";
        List<Category> listCategories = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Category category = new Category();
            category.setId(rs.getInt("id"));
            category.setName(rs.getString("name"));
            category.setDescription(rs.getString("description"));
            category.setAvailability(rs.getBoolean("availability"));
            category.setParent(rs.getInt("parent"));

            category.setSubcategory(getByParent(category.getId()));

            return category;
        });

        return listCategories;
    }

    @Override
    public int getId(String name) {
        String sql = "SELECT * FROM category WHERE name='" + name + "'";
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                return rs.getInt("id");
            }
            return null;
        });
    }

    @Override
    public Category get(String name) {
        String sql = "SELECT * FROM category WHERE name='" + name + "'";
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                Category category = new Category();
                category.setId(rs.getInt("id"));
                category.setName(rs.getString("name"));
                category.setDescription(rs.getString("description"));
                category.setAvailability(rs.getBoolean("availability"));
                category.setParent(rs.getInt("parent"));

                category.setSubcategory(getByParent(category.getId()));
                return category;
            }

            return null;
        });
    }

    @Override
    public void addCategory(Category category) {
        if (existCategory(category.getName())){
            return;
        }
        category.setId(0);
        saveOrUpdate(category);
    }

    @Override
    public boolean existCategory(String name) {
        String sql = "SELECT count(1) FROM category WHERE name = ?";
        return jdbcTemplate.query(sql, new Object[]{name}, rs -> {
            if (rs.next()) {
                return (rs.getInt(1) > 0);
            }
            return  false;
        });
    }

    @Override
    public void hideCategory(int categoryId) {
        String sql = "UPDATE category SET availability=? WHERE id=?";
        jdbcTemplate.update(sql, false, categoryId);
    }

    @Override
    public void showCategory(int categoryId) {
        String sql = "UPDATE category SET availability=? WHERE id=?";
        jdbcTemplate.update(sql, true, categoryId);
    }


}

