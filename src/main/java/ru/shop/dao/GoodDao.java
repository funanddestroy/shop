package ru.shop.dao;

import ru.shop.model.Good;
import ru.shop.model.Pair;

import java.util.List;

public interface GoodDao {

    List<Good> getByCategory(int categoryId);

    List<Pair<Good, Integer>> getByUserInCart(int userId);

    List<Good> getBySeller(int sellerId);

    void addGood(Good good);

    void hideGood(int goodId);

    void showGood(int goodId);
}
