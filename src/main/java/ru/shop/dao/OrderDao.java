package ru.shop.dao;

import ru.shop.model.*;

import java.util.List;

public interface OrderDao {
    List<Order> getByUser(User user);
    List<Order> getByUserAndStatus(User user, String status);
    List<Pair<List<GoodQuantity>, User>> getOrders(String status, int sellerId);
    void createOrder(User user);
    void addGood(User user, int goodId, int quantity);
    void deleteGood(User user, int goodId);
    void ordering(User user, double price, String deliveryMethod, String paymentMethod);
    boolean isGoodInOrder(int goodId, int orderId);
}
