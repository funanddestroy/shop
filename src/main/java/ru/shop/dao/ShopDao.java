package ru.shop.dao;

import java.util.List;

public interface ShopDao<T> {

    void saveOrUpdate(T t);

    void delete(int tId);

    T get(int tId);

    List<T> list();
}
