package ru.shop.dao;

import ru.shop.model.User;

import java.sql.SQLException;

public interface UserDao {

    User getByLogin(String login);

    void addUser(User user) throws SQLException;

    boolean isValidUser(String username, String password) throws SQLException;

    boolean existUser(String username) throws SQLException;

    void setRole(int userId, String role);

}
