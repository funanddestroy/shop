package ru.shop.model;

import java.util.List;

public class Category {
    private String name;
    private String description;
    private Boolean availability;
    private List<Category> subcategory;
    private int id;
    private int parent;

    @Override
    public String toString(){
        return "id = " + id + "<br/>" +
                "{name = " + name + "<br/>" +
                " description = " + description + "<br/>" +
                " availability = " + availability + "<br/>" +
                " subcategory = " + subcategory + "}";
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public int getParent() {

        return parent;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getAvailability() {
        return this.availability;
    }

    public List<Category> getSubcategory() {
        return subcategory;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    public void setSubcategory(List<Category> subcategory) {
        this.subcategory = subcategory;
    }

    public void setId(int id) {
        this.id = id;
    }
}
