package ru.shop.model;

import org.springframework.stereotype.Component;

@Component
public class User {
    private String login;
    private String password;
    private String role;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String phone;
    private String email;
    private String postcode;
    private String address;
    private int id;

    @Override
    public String toString(){
        return "id = " + id + "<br/>" +
                "{login = " + login + "<br/>" +
                " password = " + password + "<br/>" +
                " role = " + role + "<br/>" +
                " firstName = " + firstName + "<br/>" +
                " lastName = " + lastName + "<br/>" +
                " patronymic = " + patronymic + "<br/>" +
                " phone = " + phone + "<br/>" +
                " email = " + email + "<br/>" +
                " postcode = " + postcode + "<br/>" +
                " address = " + address + "}";
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getAddress() {
        return address;
    }

    public int getId() {
        return id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setId(int id) {
        this.id = id;
    }
}
