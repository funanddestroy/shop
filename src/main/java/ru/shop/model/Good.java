package ru.shop.model;

import java.util.List;

public class Good {
    private String name;
    private String description;
    private String model;
    private double price;
    private int discount;
    private Boolean availability;
    private int quantity;
    private List<String> images;
    private String manufacturer;
    private Category category;
    private User seller;
    private int id;

    @Override
    public String toString(){
        return "id = " + id + "<br/>" +
                "{category = " + category + "<br/>" +
                " name = " + name + "<br/>" +
                " manufacturer = " + manufacturer + "<br/>" +
                " description = " + description + "<br/>" +
                " model = " + model + "<br/>" +
                " price = " + price + "<br/>" +
                " discount = " + discount + "<br/>" +
                " availability = " + availability + "<br/>" +
                " seller = " + seller + "<br/>" +
                " quantity = " + quantity + "}";
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getModel() {
        return model;
    }

    public Double getPrice() {
        return price;
    }

    public Integer getDiscount() {
        return discount;
    }

    public Boolean getAvailability() {
        return availability;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public List<String> getImages() {
        return images;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public Category getCategory() {
        return category;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }
}
