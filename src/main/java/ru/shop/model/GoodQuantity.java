package ru.shop.model;

public class GoodQuantity {
    private Good good;
    private int quantity;

    public GoodQuantity(){}

    public GoodQuantity(Good good, int quantity) {
        this.good = good;
        this.quantity = quantity;
    }

    @Override
    public String toString(){
        return  "{good = " + good + "<br/>" +
                " quantity = " + quantity + "}";
    }

    public Good getGood() {
        return good;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setGood(Good good) {
        this.good = good;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
