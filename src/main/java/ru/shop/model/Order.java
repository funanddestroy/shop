package ru.shop.model;

import java.util.List;

public class Order {
    private User user;
    private List<GoodQuantity> goods;
    private Double price;
    private String deliveryMethod;
    private String paymentMethod;
    private String status;
    private int id;

    @Override
    public String toString(){
        return  "id = " + id + "<br/>" +
                "{user = " + user + "<br/>" +
                " goods = " + goods + "<br/>" +
                " price = " + price + "<br/>" +
                " deliveryMethod = " + deliveryMethod + "<br/>" +
                " paymentMethod = " + paymentMethod + "<br/>" +
                " status = " + status + "}";
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {

        return id;
    }

    public User getUser() {
        return user;
    }

    public List<GoodQuantity> getGoods() {
        return goods;
    }

    public Double getPrice() {
        return price;
    }

    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public String getStatus() {
        return status;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setGoods(List<GoodQuantity> goods) {
        this.goods = goods;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
