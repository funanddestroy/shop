package ru.shop.service.impl;

import ru.shop.dao.OrderDao;
import ru.shop.model.*;
import ru.shop.service.OrderService;

import java.util.List;

public class OrderServiceImpl implements OrderService {

    private OrderDao orderDao;

    @Override
    public List<Order> getByUserAndStatus(User user, String status) {
        return orderDao.getByUserAndStatus(user, status);
    }

    @Override
    public void createOrder(User user) {
        orderDao.createOrder(user);
    }

    @Override
    public void addGood(User user, int goodId, int quantaty) {
        orderDao.addGood(user, goodId, quantaty);
    }

    @Override
    public void deleteGood(User user, int goodId) {
        orderDao.deleteGood(user, goodId);
    }

    @Override
    public void ordering(User user, double price, String deliveryMethod, String paymentMethod) {
        orderDao.ordering(user, price, deliveryMethod, paymentMethod);
    }

    @Override
    public List<Pair<List<GoodQuantity>, User>> getOrders(String status, int sellerId) {
        return orderDao.getOrders(status, sellerId);
    }

    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    public OrderDao getOrderDao() {
        return orderDao;
    }
}
