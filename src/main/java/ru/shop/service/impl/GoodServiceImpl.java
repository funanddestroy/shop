package ru.shop.service.impl;

import ru.shop.dao.impl.GoodDaoImpl;
import ru.shop.model.Good;
import ru.shop.service.GoodService;

import java.util.List;

public class GoodServiceImpl implements GoodService {
    private GoodDaoImpl goodDao;

    @Override
    public List<Good> getGoodsBySeller(int sellerId) {
        return goodDao.getBySeller(sellerId);
    }

    @Override
    public void addGood(Good good) {
        goodDao.addGood(good);
    }

    @Override
    public void deleteGood(int goodId) {
        goodDao.delete(goodId);
    }

    @Override
    public void hideGood(int goodId) {
        goodDao.hideGood(goodId);
    }

    @Override
    public void showGood(int goodId) {
        goodDao.showGood(goodId);
    }

    public void setGoodDao(GoodDaoImpl goodDao) {
        this.goodDao = goodDao;
    }

    public GoodDaoImpl getGoodDao() {
        return goodDao;
    }
}
