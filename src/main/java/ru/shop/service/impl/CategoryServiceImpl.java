package ru.shop.service.impl;

import ru.shop.dao.impl.CategoryDaoImpl;
import ru.shop.model.Category;
import ru.shop.service.CategoryService;

public class CategoryServiceImpl implements CategoryService {
    private CategoryDaoImpl categoryDao;

    @Override
    public void addCategory(Category category) {
        categoryDao.addCategory(category);
    }

    @Override
    public void deleteCategory(int categoryId) {
        categoryDao.delete(categoryId);
    }

    @Override
    public void hideCategory(int categoryId) {
        categoryDao.hideCategory(categoryId);
    }

    @Override
    public void showCategory(int categoryId) {
        categoryDao.showCategory(categoryId);
    }

    @Override
    public Category getByName(String name) {
        return categoryDao.get(name);
    }

    public void setCategoryDao(CategoryDaoImpl categoryDao) {
        this.categoryDao = categoryDao;
    }

    public CategoryDaoImpl getCategoryDao() {
        return categoryDao;
    }
}
