package ru.shop.service.impl;

import ru.shop.dao.UserDao;
import ru.shop.dao.impl.UserDaoImpl;
import ru.shop.model.User;
import ru.shop.service.UserService;

import java.sql.SQLException;

public class UserServiceImpl implements UserService {
    private UserDaoImpl userDao;

    public UserDao getUserDao()
    {
        return this.userDao;
    }

    public void setUserDao(UserDaoImpl userDao)
    {
        this.userDao = userDao;
    }

    @Override
    public boolean isValidUser(String username, String password) throws SQLException
    {
        return userDao.isValidUser(username, password);
    }

    @Override
    public boolean existUser(String username) throws SQLException {
        return userDao.existUser(username);
    }

    @Override
    public User getUserByLogin(String login) {
        return userDao.getByLogin(login);
    }

    @Override
    public void addUser(User user) throws SQLException {
        userDao.addUser(user);
    }

    @Override
    public void deleteUser(int userId) {
        userDao.delete(userId);
    }

    @Override
    public void setRole(int userId, String role) {
        userDao.setRole(userId, role);
    }
}
