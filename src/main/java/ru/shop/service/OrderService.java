package ru.shop.service;

import ru.shop.model.*;

import java.util.List;

public interface OrderService {
    List<Order> getByUserAndStatus(User user, String status);
    void createOrder(User user);
    void addGood(User user, int goodId, int quantaty);
    void deleteGood(User user, int goodId);
    void ordering(User user, double price, String deliveryMethod, String paymentMethod);
    List<Pair<List<GoodQuantity>, User>> getOrders(String status, int sellerId);
}
