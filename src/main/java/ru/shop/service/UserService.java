package ru.shop.service;

import ru.shop.model.User;

import java.sql.SQLException;

public interface UserService {
    boolean isValidUser(String username, String password) throws SQLException;
    boolean existUser(String username) throws SQLException;
    User getUserByLogin(String login);
    void addUser(User user) throws SQLException;
    void deleteUser(int userId);
    void setRole(int userId, String role);
}
