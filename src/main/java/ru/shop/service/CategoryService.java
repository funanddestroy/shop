package ru.shop.service;

import ru.shop.model.Category;

public interface CategoryService {
    void addCategory(Category category);
    void deleteCategory(int categoryId);
    void hideCategory(int categoryId);
    void showCategory(int categoryId);
    Category getByName(String name);
}
