package ru.shop.service;

import ru.shop.model.Good;

import java.util.List;

public interface GoodService {
    List<Good> getGoodsBySeller(int sellerId);

    void addGood(Good good);
    void deleteGood(int goodId);

    void hideGood(int goodId);

    void showGood(int goodId);
}
