package ru.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import ru.shop.dao.impl.CategoryDaoImpl;
import ru.shop.model.LoginBean;
import ru.shop.model.User;
import ru.shop.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@SessionAttributes("userJSP")
public class LoginController {
    @Autowired
    private UserService userService;
    @Autowired
    private CategoryDaoImpl categoryDao;

    @ModelAttribute("userJSP")
    public User createUser() {
        return new User();
    }

    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public ModelAndView displayLogin(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus)
    {
        ModelAndView model = new ModelAndView("login");
        LoginBean loginBean = new LoginBean();
        model.addObject("loginBeanJSP", loginBean);
        model.addObject("lc", categoryDao.get(1));
        model.addObject("userJSP", user);
        return model;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView executeLogin(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("loginBeanJSP")LoginBean loginBean, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus)
    {
        ModelAndView model= null;
        try
        {
            boolean isValidUser = userService.isValidUser(loginBean.getUserName(), loginBean.getPassword());
            if(isValidUser)
            {
                System.out.println("User Login Successful");
                User fUser = userService.getUserByLogin(loginBean.getUserName());
                request.setAttribute("loginBeanJSP", loginBean);
                model = new ModelAndView("login");
                model.addObject("lc", categoryDao.get(1));
                model.addObject("userJSP", fUser);
                model.addObject("loginSuccess", true);
                request.setAttribute("messageLoginSuccess", "Добро пожаловать, " + fUser.getFirstName() + " " + fUser.getPatronymic());
            }
            else
            {
                model = new ModelAndView("login");
                model.addObject("loginBeanJSP", loginBean);
                model.addObject("userJSP", user);
                model.addObject("lc", categoryDao.get(1));
                model.addObject("loginSuccess", false);
                request.setAttribute("message", "Неправильный логин или пароль!");
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return model;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView displayLogout(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus)
    {
        ModelAndView model = new ModelAndView("index");
        model.addObject("lc", categoryDao.get(1));
        model.addObject("userJSP", user);
        request.setAttribute("massageLogout", "Удачи, " + user.getFirstName() + " " + user.getPatronymic());

        sessionStatus.setComplete();

        return model;
    }
}
