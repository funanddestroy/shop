package ru.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import ru.shop.dao.impl.CategoryDaoImpl;
import ru.shop.model.RegisterBean;
import ru.shop.model.User;
import ru.shop.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

@Controller
@SessionAttributes("userJSP")
public class RegisterController {

    @Autowired
    private UserService userService;
    @Autowired
    private CategoryDaoImpl categoryDao;

    @ModelAttribute("userJSP")
    public User createUser() {
        return new User();
    }

    @RequestMapping(value = "/register",method = RequestMethod.GET)
    public ModelAndView displayRegister(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus)
    {
        ModelAndView model = new ModelAndView("register");
        RegisterBean registerBean = new RegisterBean();
        model.addObject("registerBeanJSP", registerBean);
        model.addObject("lc", categoryDao.get(1));
        model.addObject("userJSP", user);
        return model;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView executeLogin(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("registerBeanJSP")RegisterBean registerBean, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus)
    {
        ModelAndView model= null;
        try
        {
            boolean notExistUser = !userService.existUser(registerBean.getLogin()) && registerBean.getLogin().length() > 3;
            if(notExistUser)
            {
                boolean isValidPassword = Objects.equals(registerBean.getPassword(), registerBean.getConfirmPassword()) && registerBean.getPassword().length() > 3;
                if(isValidPassword)
                {
                    System.out.println("User Register Successful");

                    User newUser = new User();
                    newUser.setLogin(registerBean.getLogin());
                    newUser.setPassword(registerBean.getPassword());
                    newUser.setRole("user");
                    newUser.setFirstName(registerBean.getFirstName());
                    newUser.setLastName(registerBean.getLastName());
                    newUser.setPatronymic(registerBean.getPatronymic());
                    newUser.setPhone(registerBean.getPhone());
                    newUser.setEmail(registerBean.getEmail());
                    newUser.setPostcode(registerBean.getPostcode());
                    newUser.setAddress(registerBean.getAddress());
                    userService.addUser(newUser);

                    request.setAttribute("registerBeanJSP", registerBean);
                    model = new ModelAndView("register");
                    model.addObject("lc", categoryDao.get(1));
                    model.addObject("userJSP", user);
                    model.addObject("registerSuccess", true);
                    request.setAttribute("messageRegisterSuccess", "Регистрация прошла успешно");
                }
                else
                {
                    model = new ModelAndView("register");
                    model.addObject("registerBeanJSP", registerBean);
                    model.addObject("userJSP", user);
                    model.addObject("lc", categoryDao.get(1));
                    model.addObject("registerSuccess", false);
                    request.setAttribute("message", "Пароли не совпадают или пароль слишком короткий!");
                }
            }
            else
            {
                model = new ModelAndView("register");
                model.addObject("registerBeanJSP", registerBean);
                model.addObject("userJSP", user);
                model.addObject("lc", categoryDao.get(1));
                model.addObject("registerSuccess", false);
                request.setAttribute("message", "Пользователь с таким логином уже существует или слишком короткий логин!");
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return model;
    }
}
