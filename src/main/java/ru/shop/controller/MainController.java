package ru.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import ru.shop.dao.impl.CategoryDaoImpl;
import ru.shop.dao.impl.GoodDaoImpl;
import ru.shop.dao.impl.UserDaoImpl;
import ru.shop.model.*;
import ru.shop.service.CategoryService;
import ru.shop.service.GoodService;
import ru.shop.service.OrderService;
import ru.shop.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@SessionAttributes("userJSP")
public class MainController {

    @Autowired
    private CategoryDaoImpl categoryDao;
    @Autowired
    private GoodDaoImpl goodDao;
    @Autowired
    private UserDaoImpl userDao;
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;
    @Autowired
    private GoodService goodService;
    @Autowired
    private CategoryService categoryService;

    @ModelAttribute("userJSP")
    public User createUser() {
        return new User();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView main(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userJSP", user);
        modelAndView.addObject("lc", categoryDao.get(1));

        modelAndView.setViewName("index");
        return modelAndView;
    }

    @RequestMapping(value = "/category/{category}", method = RequestMethod.GET)
    public ModelAndView getPageCategory(HttpServletRequest request, HttpServletResponse response, @PathVariable("category")  String category, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userJSP", user);

        modelAndView.setViewName("category");

        if (!categoryService.getByName(category).getAvailability() && user.getRole() == null) {
            return modelAndView;
        }

        if (!categoryService.getByName(category).getAvailability() && !user.getRole().equals("admin")) {
            return modelAndView;
        }

        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("goodsJSP", goodDao.getByCategory(categoryDao.getId(category)));
        modelAndView.addObject("categoryJSP", category);

        return modelAndView;
    }

    @RequestMapping(value = "/category/{category}", method = RequestMethod.POST)
    public ModelAndView executePageCategory(HttpServletRequest request, HttpServletResponse response, @PathVariable("category") String category, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userJSP", user);
        modelAndView.setViewName("category");

        if (!categoryService.getByName(category).getAvailability() && user.getRole() == null) {
            return modelAndView;
        }

        if (!categoryService.getByName(category).getAvailability() && !user.getRole().equals("admin")) {
            return modelAndView;
        }

        String status = request.getParameter("status");
        if (status.equals("add_cart")) {
            String goodId = request.getParameter("good_id");
            String quantity = request.getParameter("quantityJSP");
            orderService.addGood(user, Integer.valueOf(goodId), Integer.valueOf(quantity));
        }

        if (status.equals("delete_good")) {
            String goodId = request.getParameter("good_id");
            goodService.deleteGood(Integer.valueOf(goodId));
        }

        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("goodsJSP", goodDao.getByCategory(categoryDao.getId(category)));
        modelAndView.addObject("categoryJSP", category);

        return modelAndView;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ModelAndView getUserList(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("users");

        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("userJSP", user);
        modelAndView.addObject("usersJSP", userDao.list());

        return modelAndView;
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ModelAndView deleteUser(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        String userId = request.getParameter("user_id");
        String status = request.getParameter("status");

        if (status.equals("delete_user")) {
            userService.deleteUser(Integer.valueOf(userId));
        }

        if (status.equals("set_role")) {
            String role = request.getParameter("roleJSP");
            userService.setRole(Integer.valueOf(userId), role);
        }

        modelAndView.setViewName("users");

        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("userJSP", user);
        modelAndView.addObject("usersJSP", userDao.list());

        return modelAndView;
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ModelAndView getUser(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("user");

        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("userJSP", user);

        return modelAndView;
    }

    @RequestMapping(value = "/goods", method = RequestMethod.GET)
    public ModelAndView getGoodList(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("goods");

        List<Pair<Good, Integer>> goodsInCart = goodDao.getByUserInCart(user.getId());

        double price = 0;
        for (Pair<Good, Integer> good: goodsInCart) {
            price += good.getElement0().getPrice() * good.getElement1();
        }

        modelAndView.addObject("priceJSP", price);
        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("userJSP", user);
        modelAndView.addObject("goodListJSP", goodsInCart);

        return modelAndView;
    }

    @RequestMapping(value = "/goods", method = RequestMethod.POST)
    public ModelAndView executeGoodList(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("goods");

        String status = request.getParameter("status");

        if (status.equals("delete")) {
            String goodId = request.getParameter("good_id");
            orderService.deleteGood(user, Integer.valueOf(goodId));
        }

        List<Pair<Good, Integer>> goodsInCart = goodDao.getByUserInCart(user.getId());

        double price = 0;
        for (Pair<Good, Integer> good: goodsInCart) {
            price += good.getElement0().getPrice() * good.getElement1();
        }

        modelAndView.addObject("priceJSP", price);
        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("userJSP", user);
        modelAndView.addObject("goodListJSP", goodsInCart);

        if (status.equals("ordering")){
            String delivery = request.getParameter("deliveryJSP");
            switch (delivery){
                case "post":
                    delivery = "Почта";
                    break;
                case "courier":
                    delivery = "Курьер";
            }

            String payment = request.getParameter("paymentJSP");
            switch (payment){
                case "cash":
                    payment = "Наложенный платеж";
                    break;
                case "card":
                    payment = "Пластиковая карта";
            }

            orderService.ordering(user, price, delivery, payment);
            modelAndView.addObject("message", "Заказ оформлен");
        }

        return modelAndView;
    }

    @RequestMapping(value = "/seller", method = RequestMethod.GET)
    public ModelAndView getSellerPage(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("seller");

        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("userJSP", user);

        modelAndView.addObject("goodsJSP", goodService.getGoodsBySeller(user.getId()));

        return modelAndView;
    }

    @RequestMapping(value = "/seller", method = RequestMethod.POST)
    public ModelAndView executeSellerPage(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("seller");

        String status = request.getParameter("status");

        if (status.equals("deleteGood")) {
            String goodId = request.getParameter("good_id");
            goodService.deleteGood(Integer.valueOf(goodId));
        }

        if (status.equals("hideGood")) {
            String goodId = request.getParameter("good_id");
            goodService.hideGood(Integer.valueOf(goodId));
        }

        if (status.equals("showGood")) {
            String goodId = request.getParameter("good_id");
            goodService.showGood(Integer.valueOf(goodId));
        }

        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("userJSP", user);

        modelAndView.addObject("goodsJSP", goodService.getGoodsBySeller(user.getId()));

        return modelAndView;
    }

    @RequestMapping(value = "/new_good", method = RequestMethod.GET)
    public ModelAndView newGood(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("new_good");

        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("userJSP", user);
        modelAndView.addObject("llcc", categoryDao.list());


        return modelAndView;
    }

    @RequestMapping(value = "/new_good", method = RequestMethod.POST)
    public ModelAndView addNewGood(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("new_good");

        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("llcc", categoryDao.list());
        modelAndView.addObject("userJSP", user);

        Good good = new Good();
        good.setId(0);
        good.setName(request.getParameter("name"));
        good.setDescription(request.getParameter("description"));
        good.setModel(request.getParameter("model"));
        good.setManufacturer(request.getParameter("manufacturer"));
        good.setPrice(Double.valueOf(request.getParameter("price")));
        good.setDiscount(Integer.valueOf(request.getParameter("discount")));
        good.setQuantity(Integer.valueOf(request.getParameter("quantity")));
        good.setCategory(categoryDao.get(Integer.valueOf(request.getParameter("category"))));
        good.setAvailability(true);
        good.setSeller(user);

        goodService.addGood(good);

        return modelAndView;
    }

    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public ModelAndView editCategory(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("edit_category");

        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("userJSP", user);


        return modelAndView;
    }

    @RequestMapping(value = "/category", method = RequestMethod.POST)
    public ModelAndView editCategoryReq(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("edit_category");

        String status = request.getParameter("status");
        if (status.equals("addCategory")) {
            String parent = request.getParameter("parent");
            Category category = new Category();
            category.setId(0);
            category.setName(request.getParameter("name"));
            category.setDescription(request.getParameter("description"));
            category.setAvailability(true);
            category.setParent(Integer.valueOf(parent));
            categoryService.addCategory(category);
        }

        if (status.equals("deleteCategory")) {
            String categoryId = request.getParameter("category_id");
            categoryService.deleteCategory(Integer.valueOf(categoryId));
        }

        if (status.equals("hideCategory")) {
            String categoryId = request.getParameter("category_id");
            categoryService.hideCategory(Integer.valueOf(categoryId));
        }

        if (status.equals("showCategory")) {
            String categoryId = request.getParameter("category_id");
            categoryService.showCategory(Integer.valueOf(categoryId));
        }

        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("userJSP", user);

        return modelAndView;
    }

    @RequestMapping(value = "/good_status", method = RequestMethod.GET)
    public ModelAndView goodStatus(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("good_status");

        modelAndView.addObject("lc", categoryDao.get(1));
        modelAndView.addObject("userJSP", user);
        modelAndView.addObject("gqJSP", orderService.getOrders("Оформлен", user.getId()));

        return modelAndView;
    }

}
