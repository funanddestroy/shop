<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Cart</title>
</head>
<body>

<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>

<br/>

<h3>Корзина</h3>

<c:if test="${userJSP.id == 0}">
    Вы не авторизованны!
</c:if>

<c:if test="${empty goodListJSP && userJSP.id != 0}">
    Корзина пуста
</c:if>

<c:if test="${not empty goodListJSP && userJSP.id != 0}">
    <h1>${message}</h1>
    <br>
    Общая стоимость: ${priceJSP}
    <br/>
    <br/>
    <c:forEach var="good" items="${goodListJSP}">
        <c:forEach var="img" items="${good.element0.images}">
            <img src="<c:url value="${img}"/>" width="200px" height="125px">
            <br/>
        </c:forEach>
        ${good.element0}
        <br/><br/>
        Количество: ${good.element1}
        <br/>
        <c:if test="${userJSP.id != 0}">
            <br/>
            <spring:form action="goods" method="post">
                <input type="hidden" name="status" value="delete"/>
                <input type="hidden" name="good_id" value="${good.element0.id}"/>
                <input type="submit" value="Удалить из корзины"/>
            </spring:form>
        </c:if>
        <br/>
        <br/>
    </c:forEach>

    <spring:form action="goods" method="post">
        <input type="hidden" name="status" value="ordering"/>
        <select name="deliveryJSP">
            <option value="post" >Почта</option>
            <option value="courier" >Курьер</option>
        </select> <br>
        <select name="paymentJSP">
            <option value="cash" >Наложенный платеж</option>
            <option value="card" >Пластиковая карта</option>
        </select> <br>
        <input type="submit" value="Оформить заказ"/>
    </spring:form>
</c:if>

</body>
</html>
