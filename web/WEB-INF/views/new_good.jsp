<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>New Good</title>
</head>
<body>

<%@ include file="header.jsp" %>

<c:if test='${userJSP.role.equals("admin") || userJSP.role.equals("seller")}'>

    <spring:form id="newGoodForm" method="post" action="new_good">

        <label>Название</label><br>
        <input type="text" name="name"/><br>
        <label>Описание</label><br>
        <input type="text" name="description"/><br>
        <label>Модель</label><br>
        <input type="text" name="model"/><br>
        <label>Производитель</label><br>
        <input type="text" name="manufacturer"/><br>
        <label>Цена</label><br>
        <input type="number" name="price" min="0.01" max="1000000" step="0.01"/><br>
        <label>Скидка</label><br>
        <input type="number" name="discount" min="0" max="100" step="1"/><br>
        <label>Количество</label><br>
        <input type="number" name="quantity" min="1" max="1000" step="1"/><br>
        <label>Категория</label><br>
        <select name="category">
            <c:forEach var="category" items="${llcc}">
                <c:if test="${empty category.subcategory}">
                    <option value="${category.id}" >${category.name}</option>
                </c:if>
            </c:forEach>
        </select> <br>
        <input type="submit" value="Добавить" />

    </spring:form>
</c:if>

</body>
</html>
